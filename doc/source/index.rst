.. pks documentation master file, created by
   sphinx-quickstart on Tue Jan 10 09:58:52 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pks documentation!
======================================

* pks (probabilistic k-swap) is a python package that provides a MCMC method to generate a uniform sample of graphs under certain constraints.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   development
   benchmark
   python/index
