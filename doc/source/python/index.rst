.. _python:

Python API Documentation
========================

* Python API

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Graph
   MarkovChain
   Stat
