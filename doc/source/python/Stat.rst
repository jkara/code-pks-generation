.. _Stat_class:

Stat
~~~~

.. contents:: Contents
   :local:

Stat Class
----------

.. automodule:: kedgeswap.Stat
    :members:
    :inherited-members:
    :undoc-members:
