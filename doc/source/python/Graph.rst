.. _Graph_class:

Graph
~~~~~

.. contents:: Contents
   :local:

Graph Class
-----------

.. automodule:: kedgeswap.Graph
    :members:
    :inherited-members:
    :undoc-members:
