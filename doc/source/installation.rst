.. _installation:

Installation
============

Requirements
------------

- pip and setuptools. To upgrade to final version : pip install \-\-upgrade pip setuptools wheel
- arch : pip install arch
- numpy
- argparse
- progressbar
- pytest
- joblib
- ipdb (not needed but useful for debugging)

Installation
------------

- To install the package :
  
    pip install ./

- Check if package works as intended: 

    pytest tests/test.py
